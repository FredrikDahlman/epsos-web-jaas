package se.sb.epsos.web.auth.jaas;


import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created:
 * Date: 2013-06-04
 * Time: 15:48
 */
public class UserLoginTest {
    @Test
    public void testLoginParameterParser() throws Exception {
        final UserLogin userLogin = new UserLogin("donald@monkeyworld.com", "1234ccccccbjnbutfihiddufgclthulljnjvjrgkhtrcfkvu");
        assertEquals("donald@monkeyworld.com", userLogin.getUsername());
        assertEquals("1234", userLogin.getPinCode());
        assertEquals("ccccccbjnbut", userLogin.getYobikeyID());
        assertEquals("ccccccbjnbutfihiddufgclthulljnjvjrgkhtrcfkvu", userLogin.getYubikeyOTP());
    }
    @Test
    public void testLocallyLoginNoPassword(){
        final UserLogin userLogin = new UserLogin("donald@monkeyworld.com", "");
        assertFalse(userLogin.validateUserLocally());
    }
    @Test
    public void testLocallyLoginNullPassword(){
        final UserLogin userLogin = new UserLogin("donald@monkeyworld.com", null);
        assertFalse(userLogin.validateUserLocally());
    }
    @Test
    public void testLocallyLoginOnlyPinCode(){
        final UserLogin userLogin = new UserLogin("donald@monkeyworld.com", "1234");
        assertFalse(userLogin.validateUserLocally());
    }
    @Test
    public void testLocallyLoginNoUsername(){
        final UserLogin userLogin = new UserLogin("", "1234ccccccbjnbutfihiddufgclthulljnjvjrgkhtrcfkvu");
        assertFalse(userLogin.validateUserLocally());
    }
    @Test
    public void testLocallyLoginNullUsername(){
        final UserLogin userLogin = new UserLogin(null, "1234ccccccbjnbutfihiddufgclthulljnjvjrgkhtrcfkvu");
        assertFalse(userLogin.validateUserLocally());
    }
    @Test
    public void testLocallyLoginSuccess(){
        final UserLogin userLogin = new UserLogin("donald@monkeyworld.com", "1234ccccccbjnbutfihiddufgclthulljnjvjrgkhtrcfkvu");
        assertTrue(userLogin.validateUserLocally());
    }
    @Test
    public void testLocallyLoginWrongUsername(){
        final UserLogin userLogin = new UserLogin("nisse@monkeyworld.com", "1234ccccccbjnbutfihiddufgclthulljnjvjrgkhtrcfkvu");
        assertFalse(userLogin.validateUserLocally());
    }
    @Test
    public void testLocallyLoginWrongPinCode(){
        final UserLogin userLogin = new UserLogin("donald@monkeyworld.com", "1235ccccccbjnbutfihiddufgclthulljnjvjrgkhtrcfkvu");
        assertFalse(userLogin.validateUserLocally());
    }
    @Test
    public void testLocallyLoginWrongYubikeyID(){
        final UserLogin userLogin = new UserLogin("donald@monkeyworld.com", "1234acccccbjnbutfihiddufgclthulljnjvjrgkhtrcfkvu");
        assertFalse(userLogin.validateUserLocally());
    }
    @Test
    public void testLoginWithReplayedOTP(){
        final UserLogin userLogin = new UserLogin("donald@monkeyworld.com", "1234ccccccbjnbutfihiddufgclthulljnjvjrgkhtrcfkvu");
        assertFalse(userLogin.validateUser());
    }


}

