package se.sb.epsos.web.auth.jaas;

import com.yubico.client.v2.YubicoResponseStatus;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created:
 * Date: 2013-06-03
 * Time: 10:50
 */
public class YubiKeyHandlerTest {
    @Test
    public void testReplayOfValidOTP() throws Exception {
        String OTP = "ccccccbjnbutfgnfevihjukugkitfchucuklnjbitckk";
        final YubiKeyHandler yubiKeyHandler = new YubiKeyHandler(OTP);
        final Boolean isValid = yubiKeyHandler.isValid();
        assertEquals(false,isValid);
        assertEquals(YubicoResponseStatus.REPLAYED_OTP, yubiKeyHandler.getResponseStatus());
    }
    @Test
    public void testIsInvalidOTP() throws Exception {
        String OTP = "ccccccbjnbutfgnfevihjukugkitfchucuklnjbitckf";
        final YubiKeyHandler yubiKeyHandler = new YubiKeyHandler(OTP);
        final Boolean isValid = yubiKeyHandler.isValid();
        assertEquals(false,isValid);
        assertEquals(YubicoResponseStatus.BAD_OTP, yubiKeyHandler.getResponseStatus());
    }
    @Test
    public void testInvalideOTPSize() throws Exception {
        String OTP = "ccccccbjnbutfgnfevihjukugkitfchucukl";
        final YubiKeyHandler yubiKeyHandler = new YubiKeyHandler(OTP);
        final Boolean isValid = yubiKeyHandler.isValid();
        assertEquals(false,isValid);
        assertEquals( YubicoResponseStatus.BAD_OTP, yubiKeyHandler.getResponseStatus());
    }
    @Test
    public void testGetId() throws Exception {
        String key = "ccccccbjnbutfgnfevihjukugkitfchucukl";
        final YubiKeyHandler yubiKeyHandler = new YubiKeyHandler(key);
        assertEquals( "ccccccbjnbut", yubiKeyHandler.getOTPId());
    }
    @Test(expected=IllegalArgumentException.class)
    public void testValidateNullOTP(){
        String OTP = null;
        final YubiKeyHandler yubiKeyHandler = new YubiKeyHandler(OTP);
        yubiKeyHandler.isValid();
    }
    @Test
    public void testgetIdFromNullOTP(){
        String OTP = null;
        final YubiKeyHandler yubiKeyHandler = new YubiKeyHandler(OTP);
        final String otpId = yubiKeyHandler.getOTPId();
        assertEquals(null,otpId);
    }
    @Test
    public void testgetIdFromTooShortOTP(){
        String OTP = "hejHopp";
        final YubiKeyHandler yubiKeyHandler = new YubiKeyHandler(OTP);
        final String otpId = yubiKeyHandler.getOTPId();
        assertEquals(null,otpId);
    }
}
