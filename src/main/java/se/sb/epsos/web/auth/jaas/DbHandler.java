package se.sb.epsos.web.auth.jaas;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.sb.epsos.web.auth.jaas.db.UserEntity;

import java.io.IOException;
import java.sql.*;
import java.util.Properties;

/**
 * Created:
 * Date: 2013-06-27
 * Time: 15:10
 */
public class DbHandler {
    private static final Logger log = LoggerFactory.getLogger(DbHandler.class);

    Connection dbConnection;

    public DbHandler() throws SQLException, IOException, ClassNotFoundException {
        Properties prop = new Properties();
        prop.load(this.getClass().getClassLoader().getResourceAsStream("db.properties"));
        Class.forName(prop.getProperty("db.driver"));
        dbConnection = DriverManager.getConnection(
                prop.getProperty("db.url"),
                prop.getProperty("db.user"),
                prop.getProperty("db.password"));
    }

    public void closeConnection(){
        try {
            dbConnection.close();
        } catch (SQLException e) {
            log.warn(e.getMessage());
        }
    }

    public UserEntity getUser(final String id) throws SQLException {
        String sqlCmd = String.format("SELECT * FROM user where id='%s'", id);
        log.debug("SQL Query = " + sqlCmd);
        Statement stmt = dbConnection.createStatement();
        ResultSet rs = stmt.executeQuery(sqlCmd);
        return getAsUserEntity(rs);
    }

    private UserEntity getAsUserEntity(ResultSet rs) throws SQLException {
        UserEntity userEntity = null;
        if (rs.next()) {
            userEntity = new UserEntity();
            userEntity.setId(rs.getString("id"));
            userEntity.setFirstname(rs.getString("firstname"));
            userEntity.setLastname(rs.getString("lastname"));
            userEntity.setKeyid(rs.getString("keyid"));
            userEntity.setPassword(rs.getString("password"));
            userEntity.setPharmacy(rs.getString("pharmacy"));
            log.debug("Found user: " + userEntity.toString());
        }
        return userEntity;
    }
}
