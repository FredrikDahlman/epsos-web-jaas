package se.sb.epsos.web.auth.jaas;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.sb.epsos.web.auth.jaas.db.UserEntity;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created:
 * Date: 2013-06-04
 * Time: 09:38
 */
public class UserLogin {
    private static final Logger log = LoggerFactory.getLogger(UserLogin.class);

    private static final int PINCODE_LENGTH = 4;
    private final String username;
    private final String pinCode;
    private final String yobikeyID;
    private final String yubikeyOTP;
    private final YubiKeyHandler yubiKeyHandler;

    public UserLogin(String username, String password) {
        this.username = username;
        pinCode = parsePinCode(password);
        yubikeyOTP = parseYubikeyOTP(password);
        yubiKeyHandler = new YubiKeyHandler(this.yubikeyOTP);
        yobikeyID = yubiKeyHandler.getYubiKeyId();
    }

    public boolean validateUser(){
        if(validateUserLocally()){
            return yubiKeyHandler.isValid();
        }
        return false;
    }

    protected boolean validateUserLocally() {
        boolean returnValue = true;
        if(username == null){
            return false;
        }
        try {
            DbHandler dbHandler = new DbHandler();
            UserEntity user = dbHandler.getUser(username);

            if (user == null) {
                log.warn("User <{}> doesen't exists in the DB", username);
                returnValue = false;
            } else if (!pinCode.equals(user.getPassword())) {
                log.warn("User <{}> doesn't have pin code <{}>", username, pinCode);
                returnValue = false;
            } else if (!yobikeyID.equals(user.getKeyid())) {
                log.warn("User <{}> doesn't have Yubikey ID <{}>", username, yobikeyID);
                returnValue = false;
            }
            dbHandler.closeConnection();
        } catch (SQLException e) {
            log.error(e.getMessage());
            returnValue = false;
        } catch (IOException e) {
            log.error(e.getMessage());
            returnValue = false;
        } catch (ClassNotFoundException e) {
            log.error(e.getMessage());
            returnValue = false;
        }
        return returnValue;
    }

    private String parseYubikeyOTP(String password) {
        if(password == null || password.equals("")){
            return "";
        }
        return password.substring(PINCODE_LENGTH, password.length());
    }

    private String parsePinCode(String password) {
        if(password == null || password.equals("")){
            return "";
        }
        return password.substring(0, PINCODE_LENGTH);
    }

    public String getUsername() {
        return username;
    }

    public String getPinCode() {
        return pinCode;
    }

    public String getYobikeyID() {
        return yobikeyID;
    }

    public String getYubikeyOTP() {
        return yubikeyOTP;
    }

    @Override
    public String toString() {
        return "UserLogin{" +
                "username='" + username + '\'' +
                ", pinCode='" + pinCode + '\'' +
                ", yobikeyID='" + yobikeyID + '\'' +
                ", yubikeyOTP='" + yubikeyOTP + '\'' +
                '}';
    }
}
