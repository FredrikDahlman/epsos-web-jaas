package se.sb.epsos.web.auth.jaas;

import java.security.Principal;

/**
 * Created:
 * Date: 2013-05-15
 * Time: 09:34
 */
public class PlainRolePrincipal implements Principal {
    private String role;
    public PlainRolePrincipal(String role) {
        this.role = role;
    }

    @Override
    public String getName() {
        return role;
    }

    @Override
    public String toString() {
        return role;
    }
}
