package se.sb.epsos.web.auth.jaas;

import javax.security.auth.Subject;
import javax.security.auth.callback.*;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created:
 * Date: 2013-05-15
 * Time: 11:25
 */
public class EpsosJaasLoginModule implements LoginModule {
    private static final Logger log = LoggerFactory.getLogger(EpsosJaasLoginModule.class);
    /**
     * Callback handler to store between initialization and authentication.
     */
    private CallbackHandler handler;

    /**
     * Subject to store.
     */
    private Subject subject;

    /**
     * Login name.
     */
    private String login;


    private List<String> userGroups;
    private PlainUserPrincipal userPrincipal;
    private PlainRolePrincipal rolePrincipal;

    /**
     * This implementation always return false.
     *
     * @see javax.security.auth.spi.LoginModule#abort()
     */
    @Override
    public boolean abort() throws LoginException {

        return false;
    }

    /**
     * This is where, should the entire authentication process succeeds,
     * principal would be set.
     *
     * @see javax.security.auth.spi.LoginModule#commit()
     */
    @Override
    public boolean commit() throws LoginException {

        try {

            userPrincipal = new PlainUserPrincipal(login);
            final boolean isUserPrincipalAdded = subject.getPrincipals().add(userPrincipal);
            log.info("Added userPrincipal:{} is added:{}", userPrincipal, isUserPrincipalAdded);

            if (userGroups != null && userGroups.size() > 0) {
                for (String groupName : userGroups) {
                    rolePrincipal = new PlainRolePrincipal(groupName);
                    boolean isRolePrincipalAdded = subject.getPrincipals().add(rolePrincipal);
                    log.info("Added rolePrincipal:{}  is added:{}", rolePrincipal, isRolePrincipalAdded);
                }
            }
            return true;

        } catch (Exception e) {

            throw new LoginException(e.getMessage());
        }
    }

    /**
     * This implementation ignores both state and options.
     *
     * @see javax.security.auth.spi.LoginModule#initialize(javax.security.auth.Subject,
     *      javax.security.auth.callback.CallbackHandler, java.util.Map,
     *      java.util.Map)
     */
    @Override
    public void initialize(Subject subject, CallbackHandler callbackHandler, Map aSharedState, Map aOptions) {

        handler = callbackHandler;
        this.subject = subject;
    }

    /**
     * This method checks whether the name and the password are the same.
     *
     * @see javax.security.auth.spi.LoginModule#login()
     */
    @Override
    public boolean login() throws LoginException {
        log.info("login");
        Callback[] callbacks = new Callback[2];
        callbacks[0] = new NameCallback("login");
        callbacks[1] = new PasswordCallback("password", true);

        try {
            logPrincipals(subject.getPrincipals());
            handler.handle(callbacks);

            String name = ((NameCallback) callbacks[0]).getName();
            String password = String.valueOf(((PasswordCallback) callbacks[1]).getPassword());
            log.info("login called, Username:{} Password:{}", name,password);
            final UserLogin userLogin = new UserLogin(name, password);
            if (!userLogin.validateUser()) {

                throw new LoginException("Authentication failed");
            }
//            if (!name.equals(password)) {
//
//                throw new LoginException("Authentication failed");
//            }

            login = name;
            userGroups = new ArrayList<String>();
            userGroups.add("admin");
            logPrincipals(subject.getPrincipals());

            return true;

        } catch (IOException e) {

            throw new LoginException(e.getMessage());

        } catch (UnsupportedCallbackException e) {

            throw new LoginException(e.getMessage());
        }
    }

/*
    private boolean validateLogin(String userName, String password){
        if(password.length() != ccccccbjnbuthtujcjnelcujtjbvvlveejthehgdulbn
                )
    }
*/

    /**
     * Clears subject from principal and credentials.
     *
     * @see javax.security.auth.spi.LoginModule#logout()
     */
    @Override
    public boolean logout() throws LoginException {
        log.info("Logout");
        try {
            logPrincipals(subject.getPrincipals());
            final boolean isUserPrincipalremoved = subject.getPrincipals().remove(userPrincipal);
            logPrincipals(subject.getPrincipals());
            log.info("Removed userPrincipal:{} is removed:{}",userPrincipal, isUserPrincipalremoved);
            if (userGroups != null && userGroups.size() > 0) {
                for (String groupName : userGroups) {
                    rolePrincipal = new PlainRolePrincipal(groupName);
                    final boolean isRolePrincipalremoved = subject.getPrincipals().remove(rolePrincipal);
                    log.info("Removed rolePrincipal:{}  is removed:{}", rolePrincipal,isRolePrincipalremoved);
                }
            }
            log.info("Logged out user: {}",userPrincipal.getName());
            userPrincipal = null;
            logPrincipals(subject.getPrincipals());
            return true;
        } catch (Exception e) {
            throw new LoginException(e.getMessage());
        }
    }

    private void logPrincipals(Set<java.security.Principal> principals){
        log.info("Stored principals");
        for (Principal principal : principals) {
            log.info("principal <{}>", principal.getName());
        }
    }

}
