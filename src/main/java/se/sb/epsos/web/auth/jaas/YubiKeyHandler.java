package se.sb.epsos.web.auth.jaas;

import com.yubico.client.v2.YubicoClient;
import com.yubico.client.v2.YubicoResponse;
import com.yubico.client.v2.YubicoResponseStatus;
import com.yubico.client.v2.exceptions.YubicoValidationException;
import com.yubico.client.v2.exceptions.YubicoValidationFailure;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Created:
 * Date: 2013-05-31
 * Time: 10:43
 */
public class YubiKeyHandler {
    private static final Logger log = LoggerFactory.getLogger(YubiKeyHandler.class);
    private static final int YUBIKEY_ID_SIZE = 12;
    //private static final int YUBIKEY_KEY_SIZE = 32;
    private static final int yubicoClintId=10221;
    private final String yubikey;
    private final YubicoClient client = YubicoClient.getClient(yubicoClintId);
    private YubicoResponseStatus yubicoResponseStatus;

    public YubiKeyHandler(String yubikey) {
        this.yubikey = yubikey;
    }

    public String getYubiKeyId(){
        if(yubikey == null || yubikey.equals("")){
            return "";
        }
        return yubikey.substring(0,YUBIKEY_ID_SIZE);
    }

    public YubicoResponseStatus getResponseStatus(){
        return yubicoResponseStatus;
    }

    public Boolean isValid(){
        yubicoResponseStatus = null;
        try {
            final YubicoResponse yubicoResponse = client.verify(yubikey);
            yubicoResponseStatus = yubicoResponse.getStatus();
            if(yubicoResponseStatus == YubicoResponseStatus.OK){
                return true;
            }else{
                log.info("Yubikey validation falied, {}", yubicoResponseStatus);
                return false;
            }
        } catch (YubicoValidationException e) {
            log.info(e.getMessage());
            return false;
        } catch (YubicoValidationFailure yubicoValidationFailure) {
            log.info(yubicoValidationFailure.getMessage());
            return false;
        }
    }

    public String getOTPId() {
        if(yubikey != null && yubikey.length() > YUBIKEY_ID_SIZE){
            return yubikey.substring(0,YUBIKEY_ID_SIZE);
        }
        return null;
    }
}
