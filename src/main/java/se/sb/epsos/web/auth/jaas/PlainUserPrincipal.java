package se.sb.epsos.web.auth.jaas;

import java.security.Principal;

/**
 * Created:
 * Date: 2013-05-15
 * Time: 09:28
 */
public class PlainUserPrincipal implements Principal {
    private String user;

    public PlainUserPrincipal(String user) {
        this.user = user;
    }

    @Override
    public String getName() {
        return user;
    }

    @Override
    public String toString() {
        return user;
    }
}
