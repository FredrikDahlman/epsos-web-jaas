-- DROP TABLE user;

CREATE TABLE user (
  id VARCHAR(255) NOT NULL PRIMARY KEY,
  firstname VARCHAR(255) NOT NULL,
  lastname VARCHAR(255) NOT NULL,
  keyid VARCHAR(40) NOT NULL,
  password VARCHAR(32) NOT NULL,
  pharmacy VARCHAR(40) NOT NULL
);

INSERT INTO user
VALUES (
  "donald@monkeyworld.com",
  "Donald",
  "Duck",
  "1234-5678",
  "e5e9fa1ba31ecd1ae84f75caaa474f3a663f05f4", -- SHA-1 hash "secret"
  "987123567123"
);

INSERT INTO user
VALUES (
  "mickey@monkeyworld.com",
  "Mickey",
  "Mouse",
  "1234-6789",
  "687656f6a192ef9c9bfa94ddd4d7ec3d513529d3", -- SHA-1 hash "mummy"
  "987123567123"
);

e5e9fa1ba31ecd1ae84f75caaa474f3a663f05f4